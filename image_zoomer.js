/*  eslint-disable max-classes-per-file */

import { encodeData, decodeData } from 'lib/base64';
import ImageViewer, { Gallery } from 'iv-viewer';
import Turbolinks from 'turbolinks';
import { goBackOrDefault } from './common_interface';

const { location } = window;

const EMBEDED_TEMPLATE_BODY_CLASS = 'ct-body';

function isEmbededPage() {
  return document.body.classList.contains(EMBEDED_TEMPLATE_BODY_CLASS);
}

// History class
export default class VirtualPage {
  constructor(name) {
    this.name = name;
    this.state = null;
    this.encodedData = location.hash.split(`#/${this.name}/`)[1]; //eslint-disable-line
    this.loadCurrentState();
  }

  get isValid() {
    return !!this.state;
  }

  loadCurrentState() {
    if (this.encodedData) {
      this.state = decodeData(this.encodedData);
    }
  }

  get hash() {
    const { name, state } = this;
    const release = App.raven_release || '';
    const version = release.substr(-4) || App.assets_version;
    return `/minimal/${version}#/${name}/${encodeData(state)}`;
  }
}

function openImageZoom({ url, downloadUrl, referrer }) {
  const viewer = new ImageViewer();
  viewer
    .on('hide', () => goBackOrDefault(referrer))
    .show(url);

  if (downloadUrl) {
    // eslint-disable-next-line
    viewer._elements.actions.innerHTML = `
      <a href="${downloadUrl}" download class="js-download-image"><i class="icon-cloud-download"></i></button>
    `;
  }
}

function openImageGallery({ id, url, galleryUrl, referrer }) {
  const viewer = new ImageViewer();
  viewer
    .on('hide', () => goBackOrDefault(referrer))
    .show(url);

  new Gallery({ url: galleryUrl, currentImageId: id, viewer }).load();
}

let page;
$(document)
  // eslint-disable-next-line consistent-return
  .on('turbolinks:load', () => {
    if (isEmbededPage()) { return null; }

    page = new VirtualPage('image');

    if (page.isValid) {
      if (page.state.galleryUrl) {
        return openImageGallery(page.state);
      }

      return openImageZoom(page.state);
    }

    if (location.href.includes('/minimal')) {
      return Turbolinks.visit('/');
    }
  })

  .on('click', '.js-img-zoom, .js-img-gallery', (e) => {
    if (isEmbededPage()) { return; }

    e.preventDefault();
    page.state = { ...e.currentTarget.dataset, referrer: location.href };
    Turbolinks.visit(page.hash);
  });

document.addEventListener('turbolinks:before-cache', () => {
  ImageViewer.destroyAll();
});
