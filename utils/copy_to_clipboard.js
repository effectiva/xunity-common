export default function copyToClipboard(value) {
  const $temp = $('<input style="position:absolute; left:-5000px;" />').appendTo($('.modal').length ? '.modal' : 'body'); // Bootstrap is preventing selection of any element outside of modal so we append input inside it if visible.
  $temp.val(value);
  $temp.select();
  document.execCommand('copy');
  $temp.remove();
}
