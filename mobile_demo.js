import Snack from './snackbar';
import Mobile from './mobile/adapter';

$(document)
  .on('click', '.js-mobile-demo', (e) => {
    e.preventDefault();
    try {
      const { method, val } = e.currentTarget.dataset;
      const result = Mobile[method](val) || '';
      Snack({ text: `[OK] ${method} ${result}` });
    } catch (err) {
      Snack({ text: err, customClass: 'snackbar-danger' });
    }
  })

  .on('click', '.js-mobile-demo-set-variable', () => {
    const value = $('#var_value').val();
    const name = $('#var_name').val();
    const result = Mobile.setVariable(name, value);
    Snack({ text: `[OK] setVariable ${name} ${value} ${result}` });
  })

  .on('click', '.js-mobile-demo-request-variable', () => {
    const name = $('#var_name').val();

    Mobile.requestVariable(name).then((result) => {
      Snack({ text: `[OK] requestVariable ${result}` });
    });
  });
