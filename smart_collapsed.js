/* eslint-disable max-classes-per-file */

import { timestampToDate } from 'lib/pure_utils';

// {
//   title_publisher: {
//    'cardTitle-publisherName': true
//   }
//   publisher: {
//     publisher1: [item, item, item]
//   },
// }
//
//

export class Index {
  constructor(name) {
    this.name = name;
    this.constructor.all[name] = {};
  }

  static all = {}

  has(key) {
    return key in this.container;
  }

  write(key, value) {
    this.container[key] = value;
    return value;
  }

  read(key) {
    return this.container[key];
  }

  push(key, value) {
    this.container[key] = this.container[key] || [];
    this.container[key].push(value);
    return value;
  }

  last(key, n = 1) {
    const arr = this.arrayFor(key);
    return arr ? arr[this.length(key) - n] : arr;
  }

  previous(key) {
    return this.last(key, 2);
  }

  length(key) {
    const arr = this.arrayFor(key);
    return arr ? arr.length : 0;
  }

  arrayFor(key) {
    return this.container[key];
  }

  get container() {
    return this.constructor.all[this.name];
  }

  set container(value) {
    return this.constructor.all[this.name] = value;
  }

  clear() {
    this.container = {};
  }
}

export class Item {
  constructor(attributes) {
    this.attributes = attributes;
    this.chainNext = false;
    this.chainPrev = false;
    this.locked = this.attributes.is_collapse_locked;
    this.duplicate = false;
    if (!this.locked && this.isDuplicate()) {
      this.markAsDuplicate();
      this.chain();
    }

    this.allItems.push(this);
  }

  // Class props and methods
  static all = [];

  static reset = () => {
    this.rules.forEach(rule => rule.index.clear());
    this.all = [];
  };

  // Instance props and methods

  get allItems() {
    return this.constructor.all;
  }

  onMarkAsDuplicate() { }

  markAsDuplicate() {
    this.duplicate = true;
    this.onMarkAsDuplicate();
  }

  isDuplicate() {
    return this.constructor.rules.some(rule => rule.applicable(this));
  }

  shouldChain() {
    if (!this.prev) { return false; }

    return this.attributes.card_publisher === this.prev.attributes.card_publisher;
  }

  chain() {
    if (this.shouldChain()) {
      this.chainPrev = true;

      if (!this.prev.chainPrev) {
        this.prev.chainNext = true;
      }
    }
  }

  get prev() {
    return this.constructor.all[this.constructor.all.length - 1];
  }
}

export class ItemDom extends Item {
  static parseElement = (el) => {
    const $el = $(el);
    const obj = {
      id: $el.data('id'),
      is_collapse_locked: $el.data('isCollapseLocked'),
      card_title: $('.card-title', $el).text(),
      card_publisher: $el.data('originPublisher'),
      date: timestampToDate($('.publish-at', $el).data('timeago')),
      target: el,
    };
    return obj;
  }

  static fromElement = (el) => {
    if (!el) { return false; }
    const parsed = this.parseElement(el);
    return new this(parsed);
  }

  get target() {
    return this.attributes.target;
  }

  isDuplicate() {
    return !this.isInjected && super.isDuplicate();
  }

  get isInjected() {
    return this.target.classList.contains('injected');
  }

  onMarkAsDuplicate() {
    this.collapse();
  }

  addClass(name) {
    this.target.classList.add(name);
  }

  hasClass(name) {
    this.target.classList.contains(name);
  }

  chain() {
    super.chain();

    this.chainPrev && this.addClass('card-chain-prev');
    this.prev && this.prev.chainNext && this.prev.addClass('card-chain-next');
  }

  collapse() {
    this.addClass('card-smart-collapsed');
  }
}

export class Rule {
  constructor(name, validator) {
    this.name = name;
    this.validator = validator;
    this.index = new Index(name);
  }

  applicable(item) {
    return this.validator(this, item);
  }
}
