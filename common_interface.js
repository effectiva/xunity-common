/* eslint-disable import/prefer-default-export */

import copyToClipboard from '@effectivastudio/copy_to_clipboard';
import Turbolinks from 'turbolinks';
import I18n from 'i18n-js';
import Mobile from './mobile/adapter';
import Snack from './snackbar';
/**
 * Turbolinks actions - 08.07.2020. Update
 *
 * Device    | Visit | Replace     | Restore | Back
 * -----------------------------------------------------------
 * Browser   | yes   | yes         | yes     | history.back();
 * iOS       | yes   | yes         | yes     | custom
 * Android   | yes   | custom      | yes     | history.back();
 *
 * * custom - needs to be implemented on application side
 *
 */

const nativeBack = () => window.history.back();
const turbolinksVisitUrlOrRoot = url => Turbolinks.visit(url || '/', { action: 'replace' });
const hasHistory = () => {
  const { currentVisit } = Turbolinks.controller;
  return currentVisit && currentVisit.referrer.absoluteURL.includes(window.location.host);
};

function commonCopyToClipboard(value) {
  if (Mobile.IS_MOBILE) {
    Mobile.copyToClipboard(value);
  } else {
    copyToClipboard(value);
  }
  Snack({ text: I18n.t('snack.copied') });
}

// Desktop is solved with download attribute and nginx config
/*
  if ( $arg_download = "1" ) {
     add_header Content-Disposition 'attachment; filename="$request_filename"';
  }
*/
export function commonDownloadImage(e, fileUrl) {
  if (Mobile.IS_MOBILE) {
    e.preventDefault();
    const url = fileUrl || e.currentTarget.dataset.val || e.currentTarget.href;
    Mobile.downloadImage(url);
  }

  Snack({ text: I18n.t('snack.downloading') });
}

let longPressTimeout;

if (Mobile.IS_ANDROID && Mobile.APP_VERSION >= '2.3.4') {
  $(document)
    .on('touchstart', 'a', (e) => {
      longPressTimeout = setTimeout(() => {
        e.preventDefault();
        Mobile.showUrlDialog(e.currentTarget.href);
      }, 800);
    })
    .on('touchmove', 'a', () => {
      clearTimeout(longPressTimeout);
    })
    .on('touchend', 'a', () => {
      clearTimeout(longPressTimeout);
    });
}

export function goBack() {
  if (Mobile.IS_MOBILE) {
    Mobile.back();
  } else {
    nativeBack();
  }
}

export function goBackOrDefault(url) {
  // Web
  if (!Mobile.IS_MOBILE) {
    return hasHistory() ? nativeBack() : turbolinksVisitUrlOrRoot(url);
  }

  // Mobile Apps
  if (Mobile.APP_VERSION <= Mobile.APP_OLD_VERSION) {
    return hasHistory() ? nativeBack() : turbolinksVisitUrlOrRoot(url);
  }

  return goBack();
}

$(document)
  .on('click', '.js-download-image', commonDownloadImage)

  .on('click', '.js-copy-to-clipboard', (e) => {
    e.preventDefault();
    commonCopyToClipboard(e.currentTarget.dataset.val);
  })

  .on('click', '.js-copy-link', (e) => {
    e.preventDefault();
    commonCopyToClipboard(e.currentTarget.href);
  })

  .on('click', '.js-share', (e) => {
    e.preventDefault();
    const { url } = e.currentTarget.dataset;
    url && url.length && Mobile.triggerShare(url);
  })

  // TODO: After 2.3.4 remove data-history-section in layouts
  .on('click', '.js-back-or-default', (e) => {
    e.preventDefault();

    // Close dropdown
    if (App.dropDownMenuOpen) {
      App.dropDownMenuOpen = false;
      return;
    }

    goBackOrDefault(e.currentTarget.href);
  });
